package com.training3.chatapplication;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;
import android.widget.Toast;

public class Wifi_broadcast_reciever extends BroadcastReceiver {

    private WifiP2pManager wifiP2pManager;
    private WifiP2pManager.Channel mchannel;
    private WiFi_Activity wiFi_activity;

    public Wifi_broadcast_reciever(WifiP2pManager wifiP2pManager, WifiP2pManager.Channel channel, WiFi_Activity activity) {
        this.wifiP2pManager = wifiP2pManager;
        this.mchannel = channel;
        this.wiFi_activity = activity;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {

            int state=intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE,-1);



            if (state==WifiP2pManager.WIFI_P2P_STATE_ENABLED)
            {
                Toast.makeText(context, "Wifi is ON", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Wifi is OFF", Toast.LENGTH_SHORT).show();
            }

        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {


            if (wifiP2pManager!=null)
            {
                wifiP2pManager.requestPeers(mchannel,wiFi_activity.peerListListener);
            }


        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {


            if (wifiP2pManager==null)
            {
                return;
            }

            NetworkInfo networkInfo=intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            if (networkInfo.isConnected())
            {
             wifiP2pManager.requestConnectionInfo(mchannel,wiFi_activity.connectionInfoListener);
            }
            else
            {
                WiFi_Activity.txt_connectionsts.setText("Device Disconneted");
            }

        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {

            Toast.makeText(context, "Device name Changed", Toast.LENGTH_SHORT).show();
        }
    }
}
