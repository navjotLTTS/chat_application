package com.training3.chatapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Selection_activity extends AppCompatActivity {

    private Button btn_wifi,btn_bluetooth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_activity);

        FINDVIEWBYID();
        CLICKLISTINER();

    }

    private void FINDVIEWBYID() {

        btn_bluetooth=findViewById(R.id.btn_bluetooth);
        btn_wifi=findViewById(R.id.btn_wifi);
    }

    private void CLICKLISTINER() {

        btn_wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(Selection_activity.this,WiFi_Activity.class);
                startActivity(intent);
            }
        });

        btn_bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent=new Intent(Selection_activity.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
