package com.training3.chatapplication;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class WiFi_Activity extends AppCompatActivity {

    private Button btn_onoff, btn_discover, btn_send;
    private ListView listView,listview1;
    public static TextView txt_connectionsts;
    private EditText edit_message;
    private WifiManager wifiManager;
    private WifiP2pManager wifiP2pManager;
    private WifiP2pManager.Channel channel;
    private BroadcastReceiver broadcastReceiver;
    private IntentFilter intentFilter;
    private List<WifiP2pDevice> peers = new ArrayList<>();
    WifiP2pDevice[] devicearray;
    static final int MESSAGE_READ = 1;
    public static final int MESSAGE_WRITE = 3;
    Serverclass serverclass;
    Clientclass clientclass;
    SendRecieve sendRecieve;
    Dialog dialog;
    ArrayList<String> chatmessages=new ArrayList<>();
    private ArrayAdapter<String> chatAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wi_fi_);

        FINDVIEWBYID();
        CLICKLISTINER();

        // show chat in listview
        chatAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, chatmessages);
        listview1.setAdapter(chatAdapter);
    }


    Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            switch (msg.what) {
                case MESSAGE_READ:

                    byte[] readbuffer = (byte[]) msg.obj;
                    String tempmez = new String(readbuffer, 0, msg.arg1);
                    chatmessages.add(tempmez);
                    chatAdapter.notifyDataSetChanged();
                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;

                    String writeMessage = new String(writeBuf);
                    chatmessages.add("Me: " + writeMessage);
                    chatAdapter.notifyDataSetChanged();
                    break;
            }

            return true;
        }
    });

    private void FINDVIEWBYID() {

        btn_onoff = findViewById(R.id.onOff);
        btn_discover = findViewById(R.id.discover);
        btn_send = findViewById(R.id.sendButton);
        listview1=findViewById(R.id.peerListView);
        txt_connectionsts = findViewById(R.id.connectionStatus);
        edit_message = findViewById(R.id.writeMsg);



        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        wifiP2pManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);

        channel = wifiP2pManager.initialize(this, getMainLooper(), null);

        broadcastReceiver = new Wifi_broadcast_reciever(wifiP2pManager, channel, this);

        // Perform Different Action with Broadcast Reciever
        intentFilter = new IntentFilter();
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

    }

    private void CLICKLISTINER() {

        btn_onoff.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {


                if (wifiManager.isWifiEnabled()) {
                    wifiManager.setWifiEnabled(false);
                    btn_onoff.setText("ON");
                } else {
                    wifiManager.setWifiEnabled(true);
                    btn_onoff.setText("OFF");
                }

            }
        });

        btn_discover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Search_device();
            }
        });


        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msz = edit_message.getText().toString();

                byte[] theByteArray = msz.getBytes();
                sendRecieve.write(theByteArray);
                edit_message.setText(" ");

            }
        });
    }

    // Search Peer devices
    private void Search_device() {

        wifiP2pManager.discoverPeers(channel, new WifiP2pManager.ActionListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onSuccess() {

                txt_connectionsts.setText("Discovery Started");
                SELECT_DEVICE();
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onFailure(int reason) {

                txt_connectionsts.setText("Discovery starting failuree");
            }
        });


    }

    // Select device that have searched
    private void SELECT_DEVICE() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.layout_wifi);
        dialog.setTitle("Searched Devices");


        listView = dialog.findViewById(R.id.listview_wifi);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                final WifiP2pDevice wifiP2pDevice = devicearray[position];
                WifiP2pConfig config = new WifiP2pConfig();
                config.deviceAddress = wifiP2pDevice.deviceAddress;
                wifiP2pManager.connect(channel, config, new WifiP2pManager.ActionListener() {
                    @Override
                    public void onSuccess() {

                        Toast.makeText(WiFi_Activity.this, "Connect with" + wifiP2pDevice.deviceName, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }

                    @Override
                    public void onFailure(int reason) {

                        Toast.makeText(WiFi_Activity.this, "Not connected", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
            }
        });

        dialog.show();

    }

    // Get a list of device name
    WifiP2pManager.PeerListListener peerListListener = new WifiP2pManager.PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peerslist) {

            if (!peerslist.getDeviceList().equals(peers)) ;
            {
                peers.clear();
                peers.addAll(peerslist.getDeviceList());

                String[] devicename_array = new String[peerslist.getDeviceList().size()];
                devicearray = new WifiP2pDevice[peerslist.getDeviceList().size()];
                int index = 0;

                for (WifiP2pDevice device : peerslist.getDeviceList()) {
                    devicename_array[index] = device.deviceName;
                    devicearray[index] = device;
                    index++;
                }
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, devicename_array);
                listView.setAdapter(arrayAdapter);

            }

            if (peers.size() == 0) {
                Toast.makeText(WiFi_Activity.this, "No discover show", Toast.LENGTH_SHORT).show();
            }
        }
    };

    WifiP2pManager.ConnectionInfoListener connectionInfoListener = new WifiP2pManager.ConnectionInfoListener() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onConnectionInfoAvailable(WifiP2pInfo info) {

            final InetAddress owneraddress = info.groupOwnerAddress;

            if (info.groupFormed && info.isGroupOwner) {
                txt_connectionsts.setText("Host");
                serverclass = new Serverclass();
                serverclass.start();
            } else if (info.groupFormed) {
                txt_connectionsts.setText("Client");
                clientclass = new Clientclass(owneraddress);
                clientclass.start();

            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    // Create a Server class
    public class Serverclass extends Thread {
        Socket socket;
        ServerSocket serverSocket;

        @Override
        public void run() {

            try {
                serverSocket = new ServerSocket(1111);
                socket = serverSocket.accept();
                sendRecieve = new SendRecieve(socket);
                sendRecieve.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Create a Client class
    public class Clientclass extends Thread {

        Socket socket;
        String host_address;

        Clientclass(InetAddress hostaddress) {

            host_address = hostaddress.getHostAddress();
            socket = new Socket();

        }

        @Override
        public void run() {

            try {
                socket.connect(new InetSocketAddress(host_address, 1111), 500);
                sendRecieve = new SendRecieve(socket);
                sendRecieve.start();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // Handle Send Recieve class
    private class SendRecieve extends Thread {
        private Socket socket;
        private InputStream inputStream;
        private OutputStream outputStream;

        SendRecieve(Socket skt) {
            this.socket = skt;

            try {
                inputStream = socket.getInputStream();
                outputStream = socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {

            byte[] buffer = new byte[1024];
            int bytes;

            while (socket != null) {
                try {
                    bytes = inputStream.read(buffer);
                    if (bytes > 0) {
                        handler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                    }
                } catch (IOException e) {

                    Log.d("error", e.getMessage());
                }
            }
        }


        public void write(byte[] bytes) {
            try {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                outputStream.write(bytes);
                handler.obtainMessage(MainActivity.MESSAGE_WRITE, -1, -1,
                        bytes).sendToTarget();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
